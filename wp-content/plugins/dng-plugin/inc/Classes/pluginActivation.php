<?php 
namespace DNG\Classes;

use DNG\Traits\Singleton;

class pluginActivation {

	use Singleton;

	protected function __construct() {

		// Load class.
		$this->setupHooks();
        // $this->setup();
	}

	protected function setupHooks() {

		/**
		 * Actions.
		 */
		//add_action( 'init', [ $this, 'setup' ] );

	}

	/**
	 * Setup.
	 *
	 * @return void
	 */
	public function setup() {
        echo "pluginActivation=========";
	}
}