<?php 
namespace DNG\post;

use DNG\Traits\Singleton;

class Post {

	use Singleton;

	protected function __construct() {

		// Load class.
		$this->setupHooks();
		echo "Post";
	}

	protected function setupHooks() {

		/**
		 * Actions.
		 */
		// add_action( 'after_setup_theme', [ $this, 'setupTheme' ] );

	}

	/**
	 * Setup theme.
	 *
	 * @return void
	 */
	// public function setupTheme() {

	// }
}