<?php

namespace DNG\comment;

use DNG\Traits\Singleton;

class Assets
{

	use Singleton;

	protected function __construct()
	{

		// load class.
		$this->setupHooks();
	}

	protected function setupHooks()
	{

		/**
		 * Actions.
		 */
		add_action('wp_enqueue_scripts', [$this, 'registerStyles']);
		add_action('wp_enqueue_scripts', [$this, 'registerScripts']);
	}

	public function registerStyles()
	{
		// Register styles.
		wp_register_style('Comment-plugin-css',  plugin_dir_url( __FILE__ ) . 'assets/css/Comment.min.css', [], false, 'all');

		// Enqueue Styles.
		wp_enqueue_style('Comment-plugin-css');

	}

	public function registerScripts()
	{
		// Register scripts.
		wp_register_script('Comment-plugin-js', plugin_dir_url( __FILE__ ) . '/assets/js/Comment.js', ['jquery'],  false, true);

		// Enqueue Scripts.
		wp_enqueue_script('Comment-plugin-js');


		// wp_localize_script('main-plugin-js', 'siteConfig', [
		// 	'homeUrl' => home_url('/'),
		// 	'ajaxUrl'    => admin_url('admin-ajax.php'),
		// 	'ajaxBookingNonce' => wp_create_nonce('booking_nonce'),
		// 	'ajaxAdvisoryNonce' => wp_create_nonce('advisory_nonce')
		// ]);
	}
}
