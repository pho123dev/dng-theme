<?php 
namespace DNG\Classes;

use DNG\Traits\Singleton;

class pluginDeactivation {

	use Singleton;

	protected function __construct() {

		// Load class.
		$this->setupHooks();
	}

	protected function setupHooks() {

		/**
		 * Actions.
		 */
        // add_action( 'init', [ $this, 'setup' ] );

	}

	/**
	 * Setup.
	 *
	 * @return void
	 */
	// public function setup() {
    //     echo "pluginDeactivation=========";
	// }
}