<?php
/**
 * Bootstraps the Theme.
 *
 * @package DNG
 */

namespace DNG\Classes;

use DNG\Traits\Singleton;
use DNG\comment\Comment;
use DNG\post\Post;
use DNG\user\User;
class DNGSetting {

	use Singleton;

	protected function __construct() {

		// Load class.
		Assets::get_instance();
		Comment::get_instance();
		Post::get_instance();
		User::get_instance();
		$this->setupHooks();
	}

	protected function setupHooks() {

		/**
		 * Actions.
		 */
		// add_action( 'init', [ $this, 'pluginActivationHook' ] );

	}

	/**
	 * Setup.
	 *
	 * @return void
	 */
}