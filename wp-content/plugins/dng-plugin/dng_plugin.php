<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              diemnhan.com
 * @since             1.0.0
 * @package           Dng_plugin
 *
 * @wordpress-plugin
 * Plugin Name:       DNG Function Management
 * Plugin URI:        seoulspa.vn
 * Description:       Plugin by Team WEB DNG
 * Version:           1.0.1
 * Author:            DNG
 * Author URI:        diemnhan.com
 * License:           GPL-2.0+
 * License URI:       
 * Text Domain:       dng_plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'DNG_PLUGIN_VERSION', '1.0.0' );
define( 'PLUGIN_FILE_URL', __FILE__ );

// echo '<pre>';
// print_r(PLUGIN_WITH_CLASSES__FILE__);
// echo '<pre>';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dng_plugin-activator.php
 */
require_once plugin_dir_path( PLUGIN_FILE_URL ) . 'vendor/autoload.php';
register_activation_hook( PLUGIN_FILE_URL, 'runpluginActivation');
register_deactivation_hook( PLUGIN_FILE_URL, 'runPluginDeactivation');

use DNG\Classes\pluginActivation;
use DNG\Classes\pluginDeactivation;

function runPluginActivation() {
	$pluginActivation = pluginActivation::get_instance();
	 \DNG\Classes\DNGSetting::get_instance();
	return $pluginActivation;
}
runPluginActivation();

function runPluginDeactivation() {
	$pluginDeactivation = pluginDeactivation::get_instance();
	return $pluginDeactivation;
}
runPluginDeactivation();

