<?php 
namespace DNG\user;

use DNG\Traits\Singleton;

class User {

	use Singleton;

	protected function __construct() {

		// Load class.
		$this->setupHooks();
		echo "User";
	}

	protected function setupHooks() {

		/**
		 * Actions.
		 */
		// add_action( 'after_setup_theme', [ $this, 'setupTheme' ] );

	}

	/**
	 * Setup theme.
	 *
	 * @return void
	 */
	// public function setupTheme() {

	// }
}