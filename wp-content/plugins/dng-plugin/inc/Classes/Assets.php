<?php

namespace DNG\Classes;

use DNG\Traits\Singleton;

class Assets
{

	use Singleton;

	protected function __construct()
	{

		// load class.
		$this->setupHooks();
	}

	protected function setupHooks()
	{

		/**
		 * Actions.
		 */
		add_action('admin_print_styles', [$this, 'registerStyles']);
		add_action('admin_print_scripts', [$this, 'registerScripts']);
	}

	public function registerStyles()
	{
		// Register styles.
		wp_register_style('bootstrap-plugin-css',  plugin_dir_url( __FILE__ ) . 'assets/css/bootstrap.min.css', [], false, 'all');
		wp_register_style('carousel-plugin-css',plugin_dir_url( __FILE__ ) . '/assets/libs/carousel/owl.carousel.min.css', [], false, 'all');
		wp_register_style('carousel-default-theme-plugin-css', plugin_dir_url( __FILE__ ) . '/assets/libs/carousel/owl.theme.default.min.css', [], false, 'all');
		wp_register_style('global-plugin-css', plugin_dir_url( __FILE__ ) . '/assets/css/global.css', [], false, 'all');

		// Enqueue Styles.
		wp_enqueue_style('bootstrap-plugin-css');
		wp_enqueue_style('global-plugin-css');
		wp_enqueue_style('carousel-plugin-css');
		wp_enqueue_style('carousel-default-theme-plugin-css');

		if (wp_is_mobile()) {
			wp_enqueue_style('responsive-plugin-css');
		}

	}

	public function registerScripts()
	{
		// Register scripts.
		wp_register_script('owl-carousel-plugin-js', THEME_DIR_URI . '/assets/js/owl.carousel.min.js', ['jquery'], false, true);
		wp_register_script('main-plugin-js', THEME_DIR_URI . '/assets/js/main.js', ['jquery'],  false, true);

		// Enqueue Scripts.
		wp_enqueue_script('main-plugin-js');


		// wp_localize_script('main-plugin-js', 'siteConfig', [
		// 	'homeUrl' => home_url('/'),
		// 	'ajaxUrl'    => admin_url('admin-ajax.php'),
		// 	'ajaxBookingNonce' => wp_create_nonce('booking_nonce'),
		// 	'ajaxAdvisoryNonce' => wp_create_nonce('advisory_nonce')
		// ]);
	}
}
