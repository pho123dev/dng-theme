<?php 
namespace DNG\comment;

use DNG\Traits\Singleton;
use DNG\comment\Assets;
class Comment {

	use Singleton;

	protected function __construct() {

		// Load class.
		$this->setupHooks();
		echo "Comment";
		Assets::get_instance();
	}

	protected function setupHooks() {

		/**
		 * Actions.
		 */
		// add_action( 'after_setup_theme', [ $this, 'setupTheme' ] );

	}

	/**
	 * Setup theme.
	 *
	 * @return void
	 */
	// public function setupTheme() {

	// }
}